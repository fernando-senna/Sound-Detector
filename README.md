# SoundDetector
Sound Detector - Using Integrated Circuits, LEDs, Resistors, Capacitors, &amp; a Teensy

<p>(Update) - After careful analysis, I discovered that the diode is not needed in order for the circuit to function.</p>

<h2>Project Description</h2>
<p>
For my term project, I chose to construct a sound detector. The sound detector will use an electret
microphone to detect sound and pass that signal to a pair of operational amplifiers. The first amplifier
will amplify the signal that it receives and the second amplifier will buffer that signal based on the
threshold of the circuit. Then the signal will be sent to a LED light and its brightness will depend on
the output of the second amplifier. After that the same signal from the ouput of the second operational
amplifier will be sent to a Teensy 2.0, where it will perform an analog-to-digital conversion and convert
the value read to voltage and then use that voltage to calculate the decibels of the signal. Finally, based
on the decibels calculated the proper LEDs will be turned on. If the decibels range is betweem -12 and
11, then the green LED will be turned on. If the decibel is 11 to 14, then the green and yellow LED will
be turned on. And if the decibels range is greater than or equal to 14, then the green, yellow, and red
LED will be turned on. If no sound is detected, then all the LEDs will be off.
</p>

<h2>Low-Level Description</h2>
<p>
The role of the electret microphone is to pickup noise and transmit that signal in order to different
portions of the circuit. As for the capacitor C1 and resistor R1 their role in their circuit is to serve as
an RC High-Pass filter and only allow a signal with a cut-off frequency of 15.92Hz, any signal below
that frequency will be ignored. Once the signal passes through the high-pass filter, it will go by the first
operational amplifier (MCP6002), the purpose of this amplifier is to serve as a non-inverting amplifier.
The non-inverting amplifier has resistors, R2 & R3, connected to the negative inputs because they need
to amplify the signal transmited by the electret micrphone. The signal needs to be amplified because
the output of the electret microphone is very small (mV). The gain of the operational amplifier is 101.
The output of the non-inverting amplifier is connected with resistor R3 and by a diode (1N4007) in order to prevent the signal from going back to the operational amplifier. After that the signal goes
through capacitor C2 and resistor R4, both components serve as a circuit that peaks the signal. When
the output of the non-inverting operational amplifier is high, C2 charges through R4, and C2 peaks the
signal. Then the signal goes to the other operational amplifier, which serves as a buffer amplifier where
it tracks the peak amplitude of the signal; the higher the sound, the higher the voltage. In order to
track the peak in amplitude the operational amplifier negative inputs are connected to resistors R5, R6,
and R7. All three resistors are connected in series and the terminal connections between R5 & R7 are
connected to the +5V supply. The other terminal of R7 is connected to the electret microphone in order
to buffer the signal.
Finally, the output of the buffer amplifier is connected to resistor R8 and LED1 (Blue) which will be
illuminated according to the voltage that the operational amplifier outputs. The higher the voltage, the
brighter the LED. The output of the buffer amplifier is also connected to pin F0 of the Teensy 2.0. This
pin is set as input in order to receive the signal and perform an analog-to-digital conversion. Inside the
micrcontroller, the analog signal is converted to voltage and then to decibels. Based on the decibels
the correct LEDs will be transmitting light. LED2 (Green) is connected to pin BO and resistor R9, LED3
(Yellow) is connected to pin B1 and resistor R10, and LED4 (Red) is connected to pin B3 and resistor
R11. If the decibels are greater than or equal to -11.00 and less than 11.00, then the LED2 will be
turned on. If the decibels are greater than or equal to 11 and less than 14, then LED2 & LED3 will be
turned on. If the decibels are greater than or equal to 14.00 then LED2, LED3, & LED4 will be turned
on. And if the decibels is equal to -0.00 (infinity) or any other value less than -11.00, none of the LEDs
will be turned on.
</p>
<h2>Parts List</h2>
<div style="text:align:center">
	<table border = "1">
		<tr>
			<th>Quantity</th>
			<th>Part Name</th>
			<th>Value/Part Number</th>
		</tr>
		<tr>
			<td>4</td>
			<td>Resistor</td>
			<td>100&Omega;</td>
		</tr>
		<tr>
			<td>2</td>
			<td>Resistor</td>
			<td>1k&Omega;</td>
		</tr>
		<tr>
			<td>1</td>
			<td>Resistor</td>
			<td>10k&Omega;</td>
		</tr>
		<tr>
			<td>4</td>
			<td>Resistor</td>
			<td>100k&Omega;</td>
		</tr>
		<tr>
			<td>1</td>
			<td>Capacitor</td>
			<td>0.1&mu;F</td>
		</tr>
		<tr>
			<td>1</td>
			<td>Capacitor</td>
			<td>10pF</td>
		</tr>
		<tr>
			<td>1</td>
			<td>Diode</td>
			<td>1N4007</td>	
		</tr>
		<tr>
			<td>1</td>
			<td>Op Amp</td>
			<td>MCP6002</td>
		</tr>
		<tr>
			<td>4</td>
			<td>LED</td>
			<td>Blue, Red, Yellow, &amp; Green</td>
		</tr>
		<tr>
			<td>1</td>
			<td>Teensy 2.0</td>
			<td>-</td>
		</tr>
		<tr>
			<td>1</td>
			<td>Electret Microphone</td>
			<td>-</td>
		</tr>
	</table>
</div>
<h2>Flow of Control</h2>

<img src = "https://4.bp.blogspot.com/-Bl1jBUVW5K4/V7OCZ5bDRbI/AAAAAAAAAlQ/BdX3DjwHUa8XEP3PpgJAPeJYtIAEAvsjACEw/s1600/flow_of_control.jpg" />

<h2>Schematic &amp; PCB Diagrams</h2>

<img src = "https://1.bp.blogspot.com/-X0DkRhmX4zU/V7OCZx9SCjI/AAAAAAAAAlI/KjrMSHMMEloF22l9-3-m44h4SrZhQEfRgCEw/s1600/Schematic.png" />
<br>
<img src = "https://1.bp.blogspot.com/-Ly4pAsbIRB0/V7OCZygP1yI/AAAAAAAAAlM/4zsHXcPzZMguTHMVeVIIF2KTH0OkZ2IbgCEw/s400/PCB.png"/>
<br />
